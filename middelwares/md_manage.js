'use strict'

const encoder = require('../services/encoder');
const helper = require('../services/helper');

exports.ensureManage = function (req, res, next) {
    let app_key_manage = req.get('App-Key-Manage');

    if (!app_key_manage) {
        return res.status(403).send({ status: 'Acceso denegado' });
    }

    let app_key_manage_encoded = encoder.encodeToBase64("@_" + helper.getAppData().AppConfig.APP_KEY_MANAGE + "_$")
    if (app_key_manage != app_key_manage_encoded) {
        return res.status(403).send({ status: 'Acceso denegado' });
    }

    next();

};
