'use strict'

var express = require('express');
var UserController = require('../controllers/user');

var api = express.Router();
var mdAuth = require('../middelwares/authenticated');
var mdAppSettings = require('../middelwares/md_settings');

var multipart = require('connect-multiparty');
var md_upload = multipart({ uploadDir: './uploads/users' });

api.post('/create', mdAppSettings.settings, UserController.saveUser);
api.put('/update', [mdAuth.ensureAuth, mdAppSettings.settings], UserController.updateUser);
api.post('/upload-image/:id', [mdAuth.ensureAuth, mdAppSettings.settings, md_upload], UserController.uploadImage);
api.get('/get-image/:imageFile', [mdAuth.ensureAuth, mdAppSettings.settings], UserController.getImageFile);
api.get('/filter-by-role/:role', [mdAuth.ensureAuth, mdAppSettings.settings], UserController.getUsersByRole);
api.get('/find/:id', [mdAuth.ensureAuth, mdAppSettings.settings], UserController.findById);
api.post('/login', mdAppSettings.settings, UserController.login);
api.put('/confirm-user/:id/:code', mdAppSettings.settings, UserController.confirmUser);
api.post('/recovery-password', mdAppSettings.settings, UserController.recoveryPassword);
api.post('/change-password', mdAppSettings.settings, UserController.changePassword);
api.delete('/delete-account/:type', [mdAuth.ensureAuth, mdAppSettings.settings], UserController.deleteAccount);

module.exports = api;
