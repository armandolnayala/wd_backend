'use strict'

//modelos
const Accesslink = require('../models/accesslinks');
const WDKnowledge = require('../models/wdknowledge');
const WDTag = require('../models/wdtag');
const WDProject = require('../models/wdproject');
const Login = require('../models/login');
const User = require('../models/user');

//servicios
const helper = require('../services/helper');
const applogger = require('../services/applogger');

function health(req, res) {
  res.status(200).send({ status: 'UP' });
}

function auth(req, res) {
  res.status(200).send({ status: 'Auth OK', user: req.user });
}

async function metric_users(req, res) {
  try {
    var objConfigToFind = helper.getConfigToFindByFilter(req,
      {
        operator: "OR",
        fnBuild: (queryRegex) => {
          return [
            { name: queryRegex },
            { surname: queryRegex },
            { email: queryRegex },
          ];
        }
      },
      false);

    let usersCount = await User.countDocuments(objConfigToFind.filter);

    User.find(objConfigToFind.filter, { password: 0, codeAuth: 0, __v: 0 })
      .sort((objConfigToFind.sort == null ? { name: 'asc' } : objConfigToFind.sort))
      .skip(objConfigToFind.pageOptions.page * objConfigToFind.pageOptions.limit)
      .limit(objConfigToFind.pageOptions.limit)
      .exec(async function (errFind, docs) {

        if (errFind) {
          applogger.error(applogger.errorMessage(errFind, "Error al buscar"));
          return res.status(helper.getAppData().HttpStatus.internal_error_server).send(helper.getResponseError("ERROR_FIND", null, req.locale));
        }

        if (docs) {
          let result_users = await load_user_indicators(docs);
          return res.status(helper.getAppData().HttpStatus.success).send(helper.getResponseOk("MENSAJE_SUCCESS", { count: usersCount, hasMore: objConfigToFind.pageOptions.limit == docs.length, result: result_users }, req.locale));
        }
        else {
          return res.status(helper.getAppData().HttpStatus.not_found).send(helper.getResponseError("MENSAJE_NOT_FOUND_RESULTS", null, req.locale));
        }
      });

  }
  catch (err) {
    applogger.error(applogger.errorMessage(err, "Error al buscar"));
    return res.status(helper.getAppData().HttpStatus.internal_error_server).send(helper.getResponseError("ERROR_FIND", null, req.locale));
  }

}

async function load_user_indicators(users) {

  var result = []
  for (let i = 0; i < users.length; i++) {
    let obj = users[i]._doc;
    let accesslink_count = await Accesslink.countDocuments({ $and: [{ user: { $eq: obj._id } }, { enabled: { $eq: true } }] });
    let knowledge_count = await WDKnowledge.countDocuments({ $and: [{ user: { $eq: obj._id } }, { status: { $eq: "ACTIVE" } }] });
    let tags_count = await WDTag.countDocuments({ user: { $eq: obj._id } });
    let projects_count = await WDProject.countDocuments({ $and: [{ user: { $eq: obj._id } }, { status: { $eq: "ACTIVE" } }] });
    let logins_count = await Login.countDocuments({ user: { $eq: obj._id } });

    let last_logins = await Login.find({ user: { $eq: obj._id } }, { token: 0, __v: 0, _id: 0, user: 0 })
      .sort({ loggedAtUnix: 'desc' })
      .skip(0)
      .limit(3)
      .exec();

    let item = {
      ...obj, indicators: [
        { name: "Accesslink_Count", value: accesslink_count },
        { name: "Knowledge_Count", value: knowledge_count },
        { name: "Tags_count", value: tags_count },
        { name: "Projects_count", value: projects_count },
        { name: "Login_Count", value: logins_count },
        { name: "Last_logins", value: last_logins },
      ]
    };

    result = [...result, item];
  }

  return result;
}


//Exporto el controller con sus metodos
module.exports =
{
  health,
  auth,
  metric_users,
};
