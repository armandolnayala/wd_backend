'use strict'

var express = require('express');
var ManageController = require('../controllers/manage');

var api = express.Router();
var mdAuth = require('../middelwares/authenticated');
var mdAdmin = require('../middelwares/md_admin');
var mdManage = require('../middelwares/md_manage');
var mdAppSettings = require('../middelwares/md_settings');

api.get('/health', mdAppSettings.settings, ManageController.health);
api.get('/auth', [mdAuth.ensureAuth, mdAppSettings.settings], ManageController.auth);
api.post('/metric/users', [mdAuth.ensureAuth, mdAdmin.isAdmin, mdManage.ensureManage, mdAppSettings.settings], ManageController.metric_users);

module.exports = api;
