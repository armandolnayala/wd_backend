'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var WDGroupSchema = Schema({
    name: String,
    canBeRemoved: Boolean,
    createdDate: Date,
    updatedDate: Date,
    deletedDate: Date,
    createdUnix: Number,
    updatedUnix: Number,
    deletedUnix: Number,
    user: { type: Schema.ObjectId, ref: 'User' }
});

module.exports = mongoose.model('WDGroup', WDGroupSchema);
