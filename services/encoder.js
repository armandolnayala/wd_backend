var jwt = require('./jwt');
var helper = require('./helper');

function encodeToBase64(value) {
    return Buffer.from(value).toString('base64');
}

function decodeFromBase64(value) {
    return Buffer.from(value, 'base64').toString('ascii');
}

function encode(id, description, value) {

    var iterations = helper.getAppData().AppConfig.encode_iterations;

    var jwtToEncode = jwt.encodeWDData(id, description, value);
    var encodedValue = jwtToEncode;
    for (var i = 0; i < iterations; i++) {
        encodedValue = Buffer.from(encodedValue).toString('base64');
    }

    return encodedValue;
}

function encodeByUser(user, value) {

    var iterations = helper.getAppData().AppConfig.encode_iterations;

    var userSecret = (user.sub + "_@_" + user.email);
    var encodedValue = value;
    for (var i = 0; i < iterations; i++) {
        encodedValue = Buffer.from(encodedValue).toString('base64');
        userSecret = Buffer.from(userSecret).toString('base64');
    }

    return (Buffer.from(encodedValue + "_WD_" + userSecret).toString('base64'));
}

function decode(data) {
    var iterations = helper.getAppData().AppConfig.encode_iterations;

    var decodedValue = data;
    for (var i = 0; i < iterations; i++) {
        decodedValue = Buffer.from(decodedValue, 'base64').toString('ascii');
    }

    return jwt.decodeWDData(decodedValue);
}

module.exports =
{
    encode,
    decode,
    encodeByUser,
    encodeToBase64,
    decodeFromBase64
}