'use strict'

//modelos
var ObjectId = require('mongoose').Types.ObjectId;
var User = require('../models/user');
var WDGroup = require('../models/wdgroup');
var WDDataEntity = require('../models/wddataentity');

//servicios
var helper = require('../services/helper');
var applogger = require('../services/applogger');
var encoder = require('../services/encoder');

//Mensajes
const MSG_ERROR_ENTITY_EXISTS = "ERROR_WDDATAENTITY_EXISTS";

async function save(req, res) {
    var params = req.body;
    var wdDataEntity = new WDDataEntity();
    var wdGroup = new WDGroup();

    try {
        wdDataEntity.name = params.name.toUpperCase();
        wdDataEntity.value = params.value;
        wdDataEntity.url = params.url;
        wdDataEntity.encode = params.encode;
        wdDataEntity.description = params.description;
        wdDataEntity.user = helper.getUserWithIdFromRequest(req);

        wdGroup._id = params.group;
        wdDataEntity.group = wdGroup;
        wdDataEntity = helper.setAuditDateInEntity(wdDataEntity, false);

        const entityFound = await WDDataEntity.findOne({ name: wdDataEntity.name, user: wdDataEntity.user });

        if (entityFound) {
            return res.status(helper.getAppData().HttpStatus.not_found).send(helper.getResponseError(MSG_ERROR_ENTITY_EXISTS, null, req.locale));
        }

        if (wdDataEntity.encode) {
            wdDataEntity.value = encoder.encode(wdDataEntity.user._id, wdDataEntity.name, wdDataEntity.value);
        }

        const entitySaved = await wdDataEntity.save();

        if (!entitySaved) {
            res.status(helper.getAppData().HttpStatus.internal_error_server).send(helper.getResponseError("ERROR_SAVE", null, req.locale));
        }
        else {
            res.status(helper.getAppData().HttpStatus.success)
                .send(helper.getResponseOk("MENSAJE_SUCCESS",
                    {
                        id: entitySaved._id,
                        name: entitySaved.name
                    },
                    req.locale));
        }
    }
    catch (err) {
        applogger.error(applogger.errorMessage(err, "Error al comprobar en BD"));
        return res.status(helper.getAppData().HttpStatus.internal_error_server).send(helper.getResponseError("ERROR_CHECK_DB", null, req.locale));
    }
}

async function update(req, res) {
    var entityToUpdate = req.body;

    try {

        entityToUpdate.name = entityToUpdate.name.toUpperCase()

        const entFound = await WDDataEntity.findOne({ $and: [{ name: { $eq: entityToUpdate.name } }, { user: { $eq: req.user.sub } }, { _id: { $ne: req.params.id } }] });
        if (entFound) {
            return res.status(helper.getAppData().HttpStatus.internal_error_server).send(helper.getResponseError(MSG_ERROR_ENTITY_EXISTS, null, req.locale));
        }

        const entitySaved = await WDDataEntity.findOne({ _id: req.params.id });

        //Validate encode rules
        if (entitySaved.encode) {
            if (entityToUpdate.encode && entityToUpdate.value != entitySaved.value) {
                entityToUpdate.value = encoder.encode(req.params.id, entityToUpdate.name, entityToUpdate.value);
            }

            if (!entityToUpdate.encode && entityToUpdate.value == entitySaved.value) {
                entityToUpdate.value = encoder.decode(entityToUpdate.value);
            }
        }

        if (!entitySaved.encode && entityToUpdate.encode) {
            entityToUpdate.value = encoder.encode(req.params.id, entityToUpdate.name, entityToUpdate.value);
        }

        //Update
        entityToUpdate = helper.setAuditDateInEntity(entityToUpdate, true);
        const resUpd = await WDProject.findByIdAndUpdate(req.params.id, entityToUpdate, { new: true });

        if (resUpd) {
            return res.status(helper.getAppData().HttpStatus.success).send(helper.getResponseOk("MENSAJE_SUCCESS", resUpd, req.locale));
        }
        else {
            return res.status(helper.getAppData().HttpStatus.not_found).send(helper.getResponseError("ERROR_ENTITY_NOT_FOUND", null, req.locale));
        }
    }
    catch (err) {
        applogger.error(applogger.errorMessage(err, "Error al actualizar"));
        return res.status(helper.getAppData().HttpStatus.internal_error_server).send(helper.getResponseError("ERROR_UPDATE", null, req.locale));
    }
}

async function deleteOperation(req, res) {
    var entityId = req.params.id;
    var type = req.params.type;

    try {
        let entityRemoved;
        entityRemoved = await WDDataEntity.findByIdAndRemove(entityId);


        if (entityRemoved) {
            return res.status(helper.getAppData().HttpStatus.success).send(helper.getResponseOk("MENSAJE_SUCCESS", entityRemoved, req.locale));
        }
        else {
            return res.status(helper.getAppData().HttpStatus.not_found).send(helper.getResponseError("ERROR_ENTITY_NOT_FOUND", null, req.locale));
        }
    }
    catch (err) {
        applogger.error(applogger.errorMessage(err, "Error al eliminar"));
        return res.status(helper.getAppData().HttpStatus.internal_error_server).send(helper.getResponseError("ERROR_DELETE", null, req.locale));
    }
}

async function findByFilter(req, res) {

    try {

        var entityFilter = req.body;
        var objConfigToFind = helper.getConfigToFindByFilter(req,
            {
                operator: "OR",
                fnBuild: (queryRegex) => {
                    return [
                        { name: queryRegex }
                    ];
                }
            });

        let vCount = await WDDataEntity.countDocuments(objConfigToFind.filter)

        WDDataEntity.find(objConfigToFind.filter)
            .sort((objConfigToFind.sort == null ? { name: 'asc' } : objConfigToFind.sort))
            .skip(objConfigToFind.pageOptions.page * objConfigToFind.pageOptions.limit)
            .limit(objConfigToFind.pageOptions.limit)
            .exec(function (errFind, docs) {

                if (errFind) {
                    applogger.error(applogger.errorMessage(errFind, "Error al buscar"));
                    return res.status(helper.getAppData().HttpStatus.internal_error_server).send(helper.getResponseError("ERROR_FIND", null, req.locale));
                }

                if (docs) {

                    if (!req.headers["wd-data-decode"]) {
                        return res.status(helper.getAppData().HttpStatus.success).send(helper.getResponseOk("MENSAJE_SUCCESS", { count: vCount, hasMore: objConfigToFind.pageOptions.limit == docs.length, result: docs }, req.locale));
                    }
                    else {
                        let wdDataEncode = req.headers["wd-data-decode"];

                        if (wdDataEncode && wdDataEncode == true) {
                            var wdDataResult = []

                            for (var i = 0; i < docs.length; i++) {

                                let entityWDData = docs[i]
                                if (entityWDData.encode) {
                                    let decodeData = encoder.decode(entityWDData.value)
                                    entityWDData.value = decodeData.data
                                }

                                wdDataResult = [...wdDataResult, entityWDData]
                            }

                            return res.status(helper.getAppData().HttpStatus.success).send(helper.getResponseOk("MENSAJE_SUCCESS", { count: vCount, hasMore: objConfigToFind.pageOptions.limit == docs.length, result: wdDataResult }, req.locale));
                        }
                        else {
                            return res.status(helper.getAppData().HttpStatus.success).send(helper.getResponseOk("MENSAJE_SUCCESS", { count: vCount, hasMore: objConfigToFind.pageOptions.limit == docs.length, result: docs }, req.locale));
                        }

                    }
                }
                else {
                    return res.status(helper.getAppData().HttpStatus.not_found).send(helper.getResponseError("MENSAJE_NOT_FOUND_RESULTS", null, req.locale));
                }
            });
    }
    catch (err) {
        applogger.error(applogger.errorMessage(err, "Error al buscar"));
        return res.status(helper.getAppData().HttpStatus.internal_error_server).send(helper.getResponseError("ERROR_FIND", null, req.locale));
    }

}

async function findById(req, res) {
    try {
        var entityId = req.params.id;

        const entityFound = await WDDataEntity.findById(entityId);
        if (entityFound) {

            if (!req.headers["wd-data-decode"]) {
                return res.status(helper.getAppData().HttpStatus.success).send(helper.getResponseOk("MENSAJE_SUCCESS", entityFound, req.locale));
            }
            else {
                let wdDataEncode = req.headers["wd-data-decode"];

                if (wdDataEncode && wdDataEncode == true && entityFound.encode) {
                    let decodeData = encoder.decode(entityFound.value)
                    entityFound.value = decodeData.data
                }
                return res.status(helper.getAppData().HttpStatus.success).send(helper.getResponseOk("MENSAJE_SUCCESS", entityFound, req.locale));
            }
        }
        else {
            return res.status(helper.getAppData().HttpStatus.not_found).send(helper.getResponseError("MENSAJE_NOT_FOUND_RESULTS", null, req.locale));
        }

    }
    catch (err) {
        applogger.error(applogger.errorMessage(err, "Error al buscar"));
        return res.status(helper.getAppData().HttpStatus.internal_error_server).send(helper.getResponseError("ERROR_FIND", null, req.locale));
    }
}

async function decodeOperation(req, res) {
    try {
        //Find User to Validate Password
        const userFound = await User.findById(req.user.sub);

        if (!userFound) {
            return res.status(helper.getAppData().HttpStatus.not_found).send(helper.getResponseError("ERROR_USUARIO_NOT_EXISTS", null, req.locale));
        }

        if (!req.headers["wd-data-key"]) {
            return res.status(helper.getAppData().HttpStatus.not_found).send(helper.getResponseError("ERROR_WRONG_VALUES", null, req.locale));
        }

        var entityResult = {
            decode: false
        };

        var passData = req.headers["wd-data-key"];
        passData = encoder.decodeFromBase64(passData);

        bcrypt.compare(passData, userFound.password, (err, check) => {
            if (err) {
                applogger.error(applogger.errorMessage(err, "Error al check usuario en BD"));
                res.status(helper.getAppData().HttpStatus.internal_error_server).send(helper.getResponseError("ERROR_CHECK_DB", entityResult, req.locale));
            }
            else {
                if (!check) {
                    return res.status(helper.getAppData().HttpStatus.success).send(helper.getResponseOk("ERROR_NOT_ALLOWED", entityResult, req.locale));
                }
                else {
                    entityResult.decode = true;
                    return res.status(helper.getAppData().HttpStatus.success).send(helper.getResponseOk("MENSAJE_SUCCESS", wdDataResult, req.locale));
                }
            }

        });

    }
    catch (err) {
        applogger.error(applogger.errorMessage(err, "Error al buscar"));
        return res.status(helper.getAppData().HttpStatus.internal_error_server).send(helper.getResponseError("ERROR_CHECK_DB", null, req.locale));
    }
}

module.exports =
{
  save,
  update,
  deleteOperation,
  findByFilter,
  findById,
  decodeOperation
};

