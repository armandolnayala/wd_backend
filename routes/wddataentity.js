'use strict'

var express = require('express');
var WDEntityController = require('../controllers/wddataentity');

var api = express.Router();
var mdAuth = require('../middelwares/authenticated');
var mdAppSettings = require('../middelwares/md_settings');

api.post('/create', [mdAuth.ensureAuth, mdAppSettings.settings], WDEntityController.save);
api.put('/update/:id', [mdAuth.ensureAuth, mdAppSettings.settings], WDEntityController.update);
api.delete('/delete/:id', [mdAuth.ensureAuth, mdAppSettings.settings], WDEntityController.deleteOperation);
api.post('/find', [mdAuth.ensureAuth, mdAppSettings.settings], WDEntityController.findByFilter);
api.get('/find/:id', [mdAuth.ensureAuth, mdAppSettings.settings], WDEntityController.findById);
api.post('/decode-wddata', [mdAuth.ensureAuth, mdAppSettings.settings], WDEntityController.decodeOperation);

module.exports = api;
