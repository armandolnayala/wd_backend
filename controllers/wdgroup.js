'use strict'

//modelos
var ObjectId = require('mongoose').Types.ObjectId;
var User = require('../models/user');
var WDGroup = require('../models/wdgroup');

//servicios
var helper = require('../services/helper');
var applogger = require('../services/applogger');

//Mensajes
const ERROR_WDKENTITY_EXISTS = "ERROR_WDGROUP_EXISTS";

async function save(req, res) {
    var params = req.body;
    try {

        var wdEntity = new WDGroup(params);

        wdEntity.name = wdEntity.name.toUpperCase();
        wdEntity.user = helper.getUserWithIdFromRequest(req);
        wdEntity.canBeRemoved = true;
        wdEntity = helper.setAuditDateInEntity(wdEntity, false);

        const entityFound = await WDGroup.findOne({ name: wdEntity.name, user: wdEntity.user });

        if (entityFound) {
            return res.status(helper.getAppData().HttpStatus.bad_request).send(helper.getResponseError(ERROR_WDKENTITY_EXISTS, null, req.locale));
        }

        const entitySaved = await wdEntity.save();

        if (!entitySaved) {
            res.status(helper.getAppData().HttpStatus.internal_error_server).send(helper.getResponseError("ERROR_SAVE", null, req.locale));
        }
        else {
            res.status(helper.getAppData().HttpStatus.success)
                .send(helper.getResponseOk("MENSAJE_SUCCESS",
                    {
                        id: entitySaved._id,
                        name: entitySaved.name
                    },
                    req.locale));
        }
    }
    catch (err) {
        applogger.error(applogger.errorMessage(err, "Error saving"));
        return res.status(helper.getAppData().HttpStatus.internal_error_server).send(helper.getResponseError("ERROR_SAVE", null, req.locale));
    }
}

async function update(req, res) {
    var entityToUpdate = req.body;

    try {

        if (!entityToUpdate.name) {
            return res.status(helper.getAppData().HttpStatus.bad_request).send(helper.getResponseError("ERROR_INCORRECT_DATA", null, req.locale));
        }

        entityToUpdate.name = entityToUpdate.name.toUpperCase();
        const entFound = await WDGroup.findOne({ $and: [{ name: { $eq: entityToUpdate.name } }, { user: { $eq: req.user.sub } }, { _id: { $ne: req.params.id } }] });
        if (entFound) {
            return res.status(helper.getAppData().HttpStatus.internal_error_server).send(helper.getResponseError(ERROR_WDKENTITY_EXISTS, null, req.locale));
        }

        entityToUpdate = helper.setAuditDateInEntity(entityToUpdate, true);

        const resUpd = await WDGroup.findByIdAndUpdate(req.params.id, entityToUpdate, { new: true });

        if (resUpd) {
            return res.status(helper.getAppData().HttpStatus.success).send(helper.getResponseOk("MENSAJE_SUCCESS", resUpd, req.locale));
        }
        else {
            return res.status(helper.getAppData().HttpStatus.not_found).send(helper.getResponseError("ERROR_ENTITY_NOT_FOUND", null, req.locale));
        }

    }
    catch (err) {
        applogger.error(applogger.errorMessage(err, "Error updating"));
        return res.status(helper.getAppData().HttpStatus.internal_error_server).send(helper.getResponseError("ERROR_UPDATE", null, req.locale));
    }
}

async function deleteOperation(req, res) {
    var entityId = req.params.id;

    try {
        let entityRemoved = await WDGroup.findByIdAndRemove(entityId);

        if (entityRemoved) {
            return res.status(helper.getAppData().HttpStatus.success).send(helper.getResponseOk("MENSAJE_SUCCESS", entityRemoved, req.locale));
        }
        else {
            return res.status(helper.getAppData().HttpStatus.not_found).send(helper.getResponseError("ERROR_ENTITY_NOT_FOUND", null, req.locale));
        }
    }
    catch (err) {
        applogger.error(applogger.errorMessage(err, "Error al eliminar"));
        return res.status(helper.getAppData().HttpStatus.internal_error_server).send(helper.getResponseError("ERROR_DELETE", null, req.locale));
    }
}

async function findByFilter(req, res) {

    try {

        var objConfigToFind = helper.getConfigToFindByFilter(req,
            {
                operator: "OR",
                fnBuild: (queryRegex) => {
                    return [
                        { name: queryRegex }
                    ];
                }
            });

        let vCount = await WDGroup.countDocuments(objConfigToFind.filter)

        WDGroup.find(objConfigToFind.filter)
            .sort((objConfigToFind.sort == null ? { name: 'asc' } : objConfigToFind.sort))
            .skip(objConfigToFind.pageOptions.page * objConfigToFind.pageOptions.limit)
            .limit(objConfigToFind.pageOptions.limit)
            .exec(function (errFind, docs) {

                if (errFind) {
                    applogger.error(applogger.errorMessage(errFind, "Error al buscar"));
                    return res.status(helper.getAppData().HttpStatus.internal_error_server).send(helper.getResponseError("ERROR_FIND", null, req.locale));
                }

                if (docs) {
                    return res.status(helper.getAppData().HttpStatus.success).send(helper.getResponseOk("MENSAJE_SUCCESS", { count: vCount, hasMore: objConfigToFind.pageOptions.limit == docs.length, result: docs }, req.locale));
                }
                else {
                    return res.status(helper.getAppData().HttpStatus.not_found).send(helper.getResponseError("MENSAJE_NOT_FOUND_RESULTS", null, req.locale));
                }
            });
    }
    catch (err) {
        applogger.error(applogger.errorMessage(err, "Error al buscar"));
        return res.status(helper.getAppData().HttpStatus.internal_error_server).send(helper.getResponseError("ERROR_FIND", null, req.locale));
    }
}

async function findById(req, res) {
    try {
        var entityId = req.params.id;

        const entityFound = await WDGroup.findById(entityId);
        if (entityFound) {
            return res.status(helper.getAppData().HttpStatus.success).send(helper.getResponseOk("MENSAJE_SUCCESS", entityFound, req.locale));
        }
        else {
            return res.status(helper.getAppData().HttpStatus.not_found).send(helper.getResponseError("MENSAJE_NOT_FOUND_RESULTS", null, req.locale));
        }

    }
    catch (err) {
        applogger.error(applogger.errorMessage(err, "Error al buscar"));
        return res.status(helper.getAppData().HttpStatus.internal_error_server).send(helper.getResponseError("ERROR_FIND", null, req.locale));
    }
}

module.exports =
{
    save,
    update,
    deleteOperation,
    findByFilter,
    findById
};