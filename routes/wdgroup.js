'use strict'

var express = require('express');
var WDGroupController = require('../controllers/wdgroup');

var api = express.Router();
var mdAuth = require('../middelwares/authenticated');
var mdAppSettings = require('../middelwares/md_settings');

api.post('/create', [mdAuth.ensureAuth, mdAppSettings.settings], WDGroupController.save);
api.put('/update/:id', [mdAuth.ensureAuth, mdAppSettings.settings], WDGroupController.update);
api.delete('/delete/:id', [mdAuth.ensureAuth, mdAppSettings.settings], WDGroupController.deleteOperation);
api.post('/find', [mdAuth.ensureAuth, mdAppSettings.settings], WDGroupController.findByFilter);
api.get('/find/:id', [mdAuth.ensureAuth, mdAppSettings.settings], WDGroupController.findById);

module.exports = api;
