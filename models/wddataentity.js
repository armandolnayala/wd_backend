'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var WDDataEntitySchema = Schema({
    name: String,
    value: String,
    url: String,
    encode: Boolean,
    description: String,
    createdDate: Date,
    updatedDate: Date,
    deletedDate: Date,
    createdUnix: Number,
    updatedUnix: Number,
    deletedUnix: Number,
    user: { type: Schema.ObjectId, ref: 'User' },
    group: { type: Schema.ObjectId, ref: 'WDGroup' }
});

module.exports = mongoose.model('WDDataEntity', WDDataEntitySchema);
